__doc__ = r"""# Introduction: any... what!?
anyBSM is a collection of python modules that allow to calculate
a selection of observables in perturbative quantum field theory within
arbitrary extensions of the Standard Model of particle physics (SM).
## Features
The library is written in a general way in the sense that it
  * uses the Universal FeynRules Output ($\UFO$) as input,
  * can calculate all bosonic one- and two- as well as scalar
  three-point functions up to one-loop order analytically as well as numerically,
  * can easily be extended to more external states of different
  spin and consequently
  * can easily be extended to calculate more observables.
  * All of that can be done in a wide range of renormalization
  schemes.

## Available observables
The above ingredients so far have been used to implement *generic*
calculations of the following (pseudo) observables and theoretical constraints:
  * [${\color{lightblue} \tt anyH3}$](./anyBSM/anyH3.html): The trilinear self-coupling of the SM-like
  Higgs boson $\lambda_{hhh}$ at the one-loop order,
  * [${\color{lightblue} \tt anyPerturbativeUnitarity}$](./anyBSM/anyPerturbativeUnitarity.html): the
  largest eigenvalue $|a_{J=0}|^{\\mathrm{max}}$ of the tree-level $SS\to SS$ scattering
  matrix in the high-energy limit

which can be predicted for almost any UFO model given to the code.

# Quickstart
## Installation
Installing anyBSM requires `python>=3.8`, `(g)fortran`, `cmake` as well as `pip`:
```bash
pip install anyBSM
```
To upgrade an already installed anyBSM run `pip install -U anyBSM`.

## Using the anyBSM executable
To calculate the trilinear Higgs self coupling 
in e.g. the Standard Model (SM) in a complete on-shell (OS) renormalization scheme:
```bash
>>> anyBSM SM
Calculating lambda_xxx for x=h
\lambda_hhh = 176.22855628707978

(tree-level =  187.2818;
 one-loop-genuine =  -16.6317;
 one-loop-WFRs =    3.8806;
 tadpoles = -10.6336;
 counterterm (masses) =   20.9946;
 counterterm (VEV) =   -8.6630;
 counterterm (custom) =    0.0000
)
```
To get more information execute
  * `anyBSM -h`: general help for the executables features
  * `anyBSM -l`: lists built-in models
  * `anyBSM <modelname> -h`: information on the model `<modelname>`. Example: `anyBSM THDMII -h`

## Using anyBSM as a library
```python
from anyBSM import anyBSM
print(anyBSM.built_in_models)
SM = anyBSM('SM')
SM.list_renormalization_schemes()
SM.load_renormalization_scheme('OStadpoles')
SM.set_evaluation_mode('numerical')
lamhhh = SM.lambdahhh() # calculate one-loop lamhhh
a0 = SM.eigSSSS() # calculate a_{J=0}
```
Instead of `'SM'` (or any other built-in model name) one can also provide the absolute path to any other $\UFO$ file.
The anyBSM package provides many building-blocks organized in
different submodules. *All* of them are documented below and on the
navigation on the left. The documentation can also be accessed from within live
(I)python/Jupyter sessions using the `help` and `?` commands.
A good starting point to get familiar with the library is the [anyBSM module index](./anyBSM.html).

## Using anyBSM within Mathematica
The Mathematica interface can be conveniently installed as follows:
```wolfram
Import["https://gitlab.com/anybsm/anybsm/-/raw/main/install.m"]
InstallAnyBSM[]
```
which checks for all requirements and adds the anyBSM
interface to Mathematica's `$Path` variable.
After successful installation, the interface can be used as follows:
```wolfram
<<anyBSM`
LoadModel["SM"]
lambda = lambdahhh[]
```
The result of `lambdahhh[]` is a Mathematica `Association` similar to the python `dictionary` 
result obtained by the corresponding python method or the list returned by the command line.
However, by default the analytical results (converted to valid Mathematica expressions) are returned.
A list of all available functions within Mathematica is stored in the variable
`$AnyFunctions`. Mathematica notebooks with more explanations and
examples are located in [the anyBSM examples repository](./examples/MathematicaExamples.html).  
A good starting point is the [Basic examples notebook](./examples/MathematicaExamples/BasicExamples.html).

# More Examples
The [anyBSM examples repository](./examples.html) contains
various Jupyter-notebooks which show how to produces numerical as well
as analytical results in many BSM models. Furthermore, it contains
example usages of the Mathematica interface as well as examples of
UFO model-file generations (see below).

# Built-in models
A variety of ready-to-use UFO models is already shipped
[alongside with the code](./models.html).
To learn more about the built-in models, continue reading [here](./models.html).

# Setting numerical parameters
$\anyBSM$ uses the default parameters defined in the respective $\UFO$ model.
To change e.g. the value of the top-quark mass in the example SM calculation discussed above, run
```python
SM.setparameters({'Mu3': 165})
```
Alternatively, a [LHA](https://arxiv.org/abs/hep-ph/0609017) file can be used as input,
```python
SM.setparameters('/path/to/LHA_file')
```
Some default $\UFO$ parameters are defined in case they are not found in the
$\UFO$ model. For instance, if the model does not define an external parameter
named `Qren` (used for the renormalization scale $Q_{\text{ren.}}$), the
code introduces it internally with a default value of
`Qren`=172.5 GeV. The full list of additionally introduced
parameters is discussed in $\anyHpaper$ and in the source of
[anyModel.import_parameters](./anyBSM/anyModel.html#anyModel.import_parameters)
(click on "view source").

# Exporting/Modifying (analytical) results
## Using the command line
The command line tool prints the results obtained in the
specified evaluation mode (default=numerical) which can be controlled with the flag `-e`:
```bash
>>> anyBSM --help
  -e {numerical,analytical,abbreviations}, --evaluation {numerical,analytical,abbreviations}
      numerical      insert numerical input numbers into diagrams or results from the cache
      abbreviations  uses the coupling short-names defined in the UFO model (this is how the results are saved to the cache)
      analytical     inserts all analytic Feynman rules explicitly and returns the result as string
```

With the flag `-o /path/to/file` the results are appended to the file
`/path/to/file` and not printed to stdout.
Additionally, the `--tex` option can be used to generate $\LaTeX$
output (see also [here](#latex-output)). For more sophisticated outputs, the anyBSM library should be
used.

## Using the library
The module [anyBSM.anyModel](./anyBSM/anyModel.html) is the mother root-module
providing methods for all physics calculations such as e.g. an interface to $\UFO\ $ models but also conversion of analytic results to
valid [Sympy](https://www.sympy.org) expressions. It is also possible to recursively
replace all internal (e.g. lagrangian) through external (input)
parameters (see [anyModel.SolveDependencies](./anyBSM/anyModel.html#anyModel.SolveDependencies)).
```python
SM = anyBSM('SM')
SM.set_evaluation_mode('analytical') # this populates `SM.symbols` with sympified model parameters
hself = SM.Sigma('h', simplify = False) # this returns a string
# just convert the string to sympy
SM.sympify(hself, simplify = False)
# convert to sympy and replace all internal UFO parameters with
# external ones except "vSM" (SM VEV). In addition, set the electron mass to 0
SM.SolveDependencies(hself,
    exlcude = ['vSM'],
    additional = {SM.symbols['Me1']: 0})
```
From there you can proceed as with any other [Sympy](https://www.sympy.org) expression. 

## Mathematica readable output
Analytic results obtained as sympy expressions (read above) can easily
be exported to valid mathematica expressions:
```python
from sympy import mathematica_code
mathematica_code(<sympy_expression>)
```
Note that this is not required if you are directly working with the [Mathematica
interface](#using-anybsm-within-mathematica) which is described above.

## $\LaTeX$ Output
The $\LaTeX$ output -whether it be the generated PDFs or the output in
interactive jupyter sessions- is generated from the `texname` entries
in the UFO model file. For interactive Jupyter sessions a custom Sympy printer
([anyBSM.latex.printer](./anyBSM/latex/printer.html)) is used to render results shown in Jupyter-notebooks into $\LaTeX$.
More information about e.g. generating summary PDFs or drawing Feynman diagrams can be found in the latex module description [anyBSM.latex](./anyBSM/latex.html).

# Renormalzation Schemes
Renormalization schemes are handled by the
[anyBSM.anyModel](./anyBSM/anyModel.html) class. The full functionality and all
possible flags supported in the `schemes.yml` are described
[therein](./anyBSM/anyModel.html#anyModel.load_renormalization_scheme). For the renormalization of the trilinear coupling
refer to [anyBSM.anyH3.lambdahhhCT()](./anyBSM/anyH3.html#anyH3.lambdahhhCT).

In the following we describe the renormalization procedure at the simple example of the
[Two Higgs Doublet Model II (THDMII)](./models/THDMII.html). For more complex examples we refer to the 
[built-in models](./models.html) which often come with more sophisticated schemes.
Information about the renormalisation of a model is saved in the file `schemes.yml` in the model directory.
A simple example file for the [THDMII](./models/THDMII.html) could look like this:
```yaml
SM_names:
  Top-Quark: u3
  W-Boson: Wp
  Z-Boson: Z
  Higgs-Boson: h1
  VEV: vvSM

default_scheme: OS

renormalization_schemes:
  OS:
    mass_counterterms:
      h1: OS
    VEV_counterterm: OS
  MS:
    mass_counterterms:
      h1: MS
    VEV_counterterm: MS
```
Here, the `SM_names` block defines the names of various SM fields and
parameters present in the UFO model (if not present it will be guessed as
described below). The `renormalization_schemes` block defines different
renormalisation schemes. In the present example, the scheme `OS` is defined
such that the mass of the field `h1` as well as the VEV counterterm are
renormalised in the OS scheme (all unspecified fields are assumed to be $\MS$).
Finite contributions of those counterterms are
taken into account automatically. This scheme is set as default scheme via the
`default_scheme` variable. In addition, the scheme `MS` is defined such
that the mass of the field `h1` as well as the VEV counterterm are
interpreted as $\MS$ parameters. It should be stressed that this does not mean
that all inputs are converted from OS to $\MS$ parameters but rather that the
physical interpretation of these parameters is changed from OS to $\MS$ (note
that the anyBSM library provides all tools to perform such conversions).

If a non-default scheme should be used, this can be specified during the model initialization:
```python
THDM = anyH3('THDMII', scheme = 'MS')
```
As an alternative to using schemes predefined in `schemes.yml`, renormalisation
schemes can also be generated interactively during runtime by using a new
name that is not yet used in the `schemes.yml` file for the
`scheme` argument during the model initialization or by calling e.g.
[THDM.add_renormalization_scheme](./anyBSM/anyModel.html#anyModel.add_renormalization_scheme) afterwards.
The new scheme will then be saved
into the `schemes.yml` file.
It is also possible to change the renormalisation scheme, e.g. between two calls
of `THDM.lambdahhh()`, using:
```python
THDM.load_renormalization_scheme('OS')
```
If no `schemes.yml` file is present in the $\UFO$ model directory, it will be
generated automatically upon the first creation of a renormalisation scheme
which automatically searches for all SM-like parameters (particles) based on
their numerical (mass) values (and PDG identifiers).


# Setup a new model
Models in anyBSM are fully described using the $\UFO\ $ standard.
If you are not familiar with the UFO specification, we recommend to
read its [documentation](https://arxiv.org/abs/1108.2040) as well as
to have a look at existing UFO models.

The code requires the following UFO files to be present in the model
directory which make use of the $\UFO$ object library located at [anyBSM.ufo.object_library](./anyBSM/ufo/object_library.html):
  * `particles.py`: specifies the fields present in the model using
  [object_library.Particle](./anyBSM/ufo/object_library.html#Particle) objects
  * `parameters.py`: specifies input (also called "external")
  parameters and underlying parameters (also called "internal"
  parameters) and their relation to the input parameters using
  [object_library.Parameter](./anyBSM/ufo/object_library.html#Parameter) objects
  * `lorentz.py`: specifies all Lorentz structures appearing in the
  model using [object_library.Lorentz](./anyBSM/ufo/object_library.html#Lorentz) objects. **It is
  important to note that the `lorentz.py` has to obey the conventions
  of the generic couplings defined in in $\anyHpaper$ or [here](https://gitlab.com/anybsm/anybsm/-/blob/main/bin/anyBSM_import#L27)**. If this is not the
  case, the model can be converted automatically by using the
  [model-importer](#model-importer-and-checks).
  * `vertices.py`: specifies all vertices along with their Lorentz
  structures and couplings using [object_library.Vertex](./anyBSM/ufo/object_library.html#Vertex).
  * `couplings.py`: specifies all couplings used in the `vertices.py`
  using [object_library.Coupling](./anyBSM/ufo/object_library.html#Coupling) object

all other files (including the `object_library.py` and `functions_library.py`) are ignored upon
the import of the model but are supplemented with the UFO
library used internally by anyBSM, see [anyBSM.ufo](./anyBSM/ufo.html) for more details.

Further limitations and requirements on the (UFO) model are:
  * The model needs to obey the conventions for Lorentz-structures, otherwise the model-converter needs to be issued before.
  * The SM-like particles should be defined, either via correct LHA codes or by defining at least one renormalization scheme in the [schemes.yml](#renormalzation-schemes)
  * The automatic OS renormalization of the SM VEV (`VEV_counterterm: OS` in the `schemes.yml`) will only work for models with an electroweak $\rho$ parameter of one. For other models a custom counterterm need to be implemented.
  * UFO couplings: for a correct replacement of the couplings of the string-representation of the cached results, the couplings need to be in ascending order (coup1, coup2, ... coup23, coup24;  such that e.g. coup24 is replaced before coup2). Otherwise only the caching of insertions should be used (set `anyBSM.caching = 1`.
  * For the calculation of $\lambda_{hhh}$ no external colored states are supported
  * no color reps. beyond $\textbf{1},\, \textbf{8}, \textbf{3}, \bar{\textbf{3}}$ are supported for loop-particles so far

It is neither common nor recommended to write a UFO model by hand but rather to
utilize a computer program for this task. Two well-known programs of
this kind are $\SARAH$ and $\FeynRules$. Their use-case is described in
the following.

## With $\tt SARAH$
For a detailed description of how to implement a model in $\SARAH$ itself we
refer to the 
[SARAH wiki](https://gitlab.in2p3.fr/goodsell/sarah/-/wikis/General_information_about_model_implementations).
Here, we assume that a SARAH model already exists and only describe
possible adjustments that may need to be performed before the UFO
output is generated using SARAH.

In general one can distinguish two different use-cases:
  1. **SPheno:** For very complex models such as Supersymmetric models it is
  common to first obtain a working 
  [SARAH-generated SPheno](https://gitlab.in2p3.fr/goodsell/sarah/-/wikis/SPheno_Output)
  code. The $\SPheno$ code will calculate the spectrum for a given set
  of input parameters and write it into an $\SLHA$ file. This file can
  provides the numerical values for all mixing matrices as well as
  mass eigenvalues and all necessary values of the parameters of the underlying
  theory. In this case the UFO model shall take all of those values as
  input (without knowing any of the relations between e.g. the mixing
  angles and the Lagrangian parameters). $\anyBSM$ is able to read-in the numerical
  values of a given SLHA file and automatically set UFO parameters/couplings equal to them (see [here](#setting-numerical-parameters)).
  2. **Standalone:** For simple extensions of e.g. only the scalar sector it is
  possible to analytically solve the relations between mass
  eigenvalues, mixing angles and underlying theory parameters. In this
  case one may implement those relations directly into the UFO model
  and can use it without a dependency on SPheno.

Both use-cases are supported. Case 1) has the advantage that one can
compute the mass spectrum as well as many other observables using
$\SPheno$ including higher-order corrections. However, the
perturbative predictions of the trilinear coupling show best
convergence in the on-shell scheme while $\SPheno$ relies on pure
$\MS$/$\DR$ renormalization. This makes it
difficult to combine it with the option in $\anyBSM$ to possibly renormalize (parts
of) the theory on-shell. Thus, we recommend the use-case 2. if the
situation allows for it.

### 1) A model depending on SPheno input
For scenario 1. no further modifications are required.
We assume that a working $\SPheno$ code (obtained with SARAH's
`MakeSPheno[]` command) of the model already exists. The UFO
model can immediately be generated`:
```wolfram
<<SARAH`
Start["MSSM"]
MakeUFO[
    Exclude -> {},
    IncludeEffectiveHiggsVertices -> False,
    MustInclude -> {hh, VZ, VP,VG, VWp, Ah, Hpm}
]
```
By default, $\SARAH$ excludes the generation of all quartic scalar
(SSSS) couplings in the UFO output. This is turned off with the option
`Exclude -> {}`. However, to still restrict the generation to a
minimal set of couplings required for the calculation of the trilinear
self-coupling, one can set the option `MustInclude` to only generate
vertices that include at least one of the fields given in the list
In the example above, only vertices with at least one scalar field
`hh`, `Ah` and `Hpm` or one vector boson `VZ`, `VWp`, `VP`, and `VG`
are generated but e.g. no vertices which involve only Stops.
For the calculation of $\lambda_{hhh}$ only the scalar fields (`hh`) are required
while all other listed fields are required
for a proper on-shell renormalization of the electroweak vacuum
expectation value (VEV). This is, however, only optional and
care should be taken if the UFO model is supposed to be used in other
tools such as Monte Carlo generators, too.

The function `MakeUFO` also allows to apply restrictions already at
the level of the couplings such as the assumption of no CKM mixing:
```wolfram
repls = Join[
    (#[i_, j_] :> KroneckerDelta[i, j]) & /@ {Vu, Uu, Vd, Ud,Ve, Ue},
    (#[i_, j_] :> #[i, j] KroneckerDelta[i, j]) & /@ {Yu, Yd, Ye}
];
MakeUFO[..., Replacements->repls]
```
The resulting UFO model can be placed in `~/.config/anyBSM/models/<modelname>`
and then called with e.g. `anyBSM <modelname> -e numerical -f <path to
slha file>` where `<path to slha file>` is a SLHA file obtained with
the SPheno code.

### A standalone model
In scenario 2. one needs to provide the relation between the
underlying theory parameters and the set of physical (possibly
on-shell) parameters. This is not always a unique set and therefore
cannot be easily automatized. For this manual step it is convenient to
start the $\SARAH$ model and generate those relations by performing e.g. the
diagonalization of the mass matrices analytically. Multiple examples
for all pre-implemented models are provided in [the anyBSM examples repository](https://gitlab.com/anybsm/anybsm_examples/-/tree/main/ModelfileGeneration).
Once the dependencies between internal and external parameters are
known, they need to be defined in the `parameters.m` file of the SARAH
model:
```wolfram
(*parameters.m of SM model*)
{...
{\[Lambda],  {
        Description -> "SM Higgs Selfcouplings",
        DependenceNum -> Mass[hh]^2/(v^2)
        }
},
...}
```
More sophisticated examples are found in [the anyBSM examples repository](https://gitlab.com/anybsm/anybsm_examples/-/tree/main/ModelfileGeneration).

### Preparing the model for OS vector boson masses

The default renormalization in $\SARAH$ calculates the $W$-boson mass
$M_W$ from the Fermi constant $G_F$, the $Z$-boson mass $M_Z$ and the
finestructure constant $\alpha_{QED}(0)$. To change this to rather use
$M_W$ instead of $G_F$ as an input (which is required for the option
`VEV_renormalization: OS` in the `schemes.yml`, see [here](./anyBSM/anyH3.html#anyH3.lambdahhhCT)) one
needs to adjust the `particles.m` accordingly:
```wolfram
(*partricles.m of the SM model*)
ParticleDefinitions[EWSB] = {
    {VZ,   {
        Description -> "Z-Boson",
        MassDependence -> Mass[VZ],
        Mass->91.187,
        Goldstone -> Ah
        }
    },
    {VWp,  {
        Description -> "W+ - Boson",
        MassDependence -> Mass[VWp],
        Mass-> 80.379,
        Goldstone -> Hp
        }
    },
      ...
}
```
More sophisticated examples are found in [the anyBSM examples repository](https://gitlab.com/anybsm/anybsm_examples/-/tree/main/ModelfileGeneration).


## With $\tt FeynRules$
Generating a UFO model with $\FeynRules$ is as easy as:
```wolfram
$OldDir = SetDirectory["/path/to/FeynRules-2.3/Models/MSSM"];
$FeynRulesPath = SetDirectory["/path/FeynRules-2.3"];
<< FeynRules`
SetDirectory[$OldDir];

LoadModel["mssm.fr"];

$CalcLag = False;
If[$CalcLag,
    lagr = Lag;
    Definition[lagr] >> lagrangian.dat, << lagrangian.dat
];

ReadLHAFile[Input -> "restrict.dat"];
WriteRestrictionFile[];
LoadRestriction["ZeroValues.rst"];

SetDirectory[$OldDir];
WriteUFO[lagr, Exclude4Scalars -> False, RemoveGhosts -> False, AddDecays -> False];
```

Unlike the $\SARAH$ generated UFO models, $\FeynRules$ UFO models are
not guaranteed to obey the conventions of all Lorentz-structures that
are expected by anyBSM. Thus, the use of the [model
importer](#model-importer-and-checks) after the model-file generation
is recommended.

Concrete examples are found in [the anyBSM examples repository](https://gitlab.com/anybsm/anybsm_examples/-/tree/main/ModelfileGeneration).

## Model importer and checks
The executable [anyBSM_import](https://gitlab.com/anybsm/anybsm/-/blob/main/bin/anyBSM_import) can be used to transform a given UFO
model into a UFO model which solely makes use of the Lorentz structure
expected by anyBSM. This is done by collecting all vertices
with the same external states and summing over the products of
coupling$\times$Lorentz-factor. Afterwards appropriate projectors are
applied to extract the needed Lorentz-structures.
```bash
anyBSM_import <path to UFO directory> -o <path to new UFO directory>
```
where the path `-o <...>` is the destination directory of the
resulting model. The tool will print a table with three columns:
Vertex (list of fields); couplings of the old model (along with their
Lorentz structures); couplings in the new model. The table is also
written to a tabular file called `conversion.csv` in the new model
directory and can be used for further cross-checks.
If one only wants to check if a model needs to be
converted at all, omit the `-o` option and add a `-c`. For more
information on the importer see `anyBSM_import -h`.
For a complete example have a look at the 
[BmLSM](./models/BmLSM.html#alternative-ufo-model) model as well as
appendix A.1 of $\anyHpaper$.


# References
If you use this program please cite:
  * [Precise predictions for the trilinear Higgs coupling in arbitrary models](https://arxiv.org/abs/2305.03015)  
    Bahl, Braathen, Gabelmann, Weiglein;  
    *[2305.03015](https://arxiv.org/abs/2305.03015)*

The program makes use of external physics libraries in various places:
  * For model implementations:
    * $\UFO$  
      Degrande, Duhr, Fuks, Grellscheid, Mattelaer, Reiter;  
      *Comput.Phys.Commun. 183 (2012) 1201-1214,
      [1108.2040](https://arxiv.org/abs/1108.2040)*
    * $\SARAH$  
      F. Staub;  
      *Comput.Phys.Commun. 184 (2013) 1792-1809,
      [1207.0906](https://arxiv.org/abs/1207.0906)*
    * $\FeynRules$  
      Alloul, Christensen, Degrande, Duhr, Fuks;  
      *Comput.Phys.Commun. 185 (2014) 2250-2300,
      [1310.1921](https://arxiv.org/abs/1310.1921)*
  * For the evaluation of loop integrals with non-zero external momenta:
    * $\COLLIER$  
      Denner, Dittmaier, Hofer;  
      *Comput.Phys.Commun. 212 (2017) 220-238,
      [1604.06792](https://arxiv.org/abs/1604.06792)*
  * For parsing (S)LHA files:
    * $\pylha$  
      Straub 2017;  
      *Github:
      [DavidMStraub/pylha](https://github.com/DavidMStraub/pylha)*

  * For the generation of $\LaTeX$ output:
    * TikZ-Feynman  
      Ellis;  
      *Comput.Phys.Commun. 210 (2017) 103-123,
      [1601.05437](https://arxiv.org/abs/1601.05437)*
"""
