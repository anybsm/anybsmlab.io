#!/usr/bin/env python3
from __future__ import division
from anyBSM import anyBSM
from anyBSM.latex.printer import parameter_name, particle_name
import yaml
from os import path
from pathlib import Path
from sympy.printing import latex
from prettytable import PrettyTable, ALL, MARKDOWN
from fractions import Fraction

models_repo = Path('.') / "models"
anyBSM_repo = Path('.') / "anybsm"
examples_repo = Path('.') / "examples"
docs = Path('.') / "docs"

print('imported anyBSM: ',anyBSM.module_dir)
print('using models from: ',anyBSM.models_dir)
assert models_repo.absolute() == Path(anyBSM.models_dir).absolute()

def tofrac(f):
    """ function to convert `0.333333` to `'1/3'` """
    return str(Fraction(f).limit_denominator())

def partprops(p,header=False, altname = '', model = None):
    """ function to prepare particle properties table """
    if header:
        return ['Particle',
                'UFO parameter',
                'mass [GeV]',
                '$U_{e.m.}(1)$',
                '$SU_c(3)$',
                '$S$',
                'anti',
                'auxiliary?',
                ]
    return [
        f'{altname} ${particle_name(p)}$',
        f'`{p.name}`',
        f'${parameter_name(p.mass.texname)} = {latex(model.sympify(str(p.mass.value), rational = False), symbol_names=model.texnames)}$' if p.mass.name != 'ZERO' else 0,
        tofrac(p.charge),
        p.color,
        {1: '0', 2: '1/2', 3: '1', -1: '1/2'}.get(p.spin,'NaN'),
        f'${particle_name(p.anti())}$' if not p.selfconjugate else '-',
        'Goldstone' if p.goldstone else 'Ghost' if p.spin == -1 else '-'
        ]

model = None
# for m in sorted([str(f.parent) for f in models_repo.glob('*/__init__.py')]):
for m in sorted(anyBSM.built_in_models.values()):
    """ iterate over all models and generate detailed output """
    del model
    model = anyBSM(m, quiet = True, ask = False, progress = False)
    print(f'Writing model details for {model.name}')
    try:
        model.set_evaluation_mode('numerical')
        model.setparameters()
        model.find_SM_parameters()
        model.find_SM_particles()
    except Exception as e:
        print('failed: ', e)

    model.set_evaluation_mode('analytical')
    h = model.SM_particles['Higgs-Boson']
    lamtree_int = None
    lamtree_ext = None

    if h:
        lamtree = model.process(h,h,h, momenta = [], only_topologies=['ThreePointTree'], simplify = False)['ThreePointTree']
        lamtree = f'-1*({lamtree})'
        simplify = len(lamtree) < 300
        lamtree_int = model.sympify(lamtree, simplify = simplify)
        if simplify:
            lamtree_int = lamtree_int.simplify().trigsimp(old=True).together()
        solvedep = len(lamtree) < 400
        if solvedep:
            lamtree_ext = model.SolveDependencies(lamtree_int, exclude = [model.SM_parameters['VEV'].name], simplify = simplify)
            if simplify:
                lamtree_ext = lamtree_ext.simplify().trigsimp(old=True).together()
            lamtree_ext = latex(lamtree_ext, symbol_names = model.texnames)
        lamtree_int = latex(lamtree_int, symbol_names = model.texnames)
        # lamnlo = model.lambdahhh()

    lamtree_ext = fr"""In terms of input (`external`) parameters:
    $$\lambda_{{hhh}}^{{(0)}} = {lamtree_ext}$$""" if lamtree_ext else ''
    lamtree_int = fr"""In terms of lagrangian (`internal`) parameters and mixing matrices:
    $$\lambda_{{hhh}}^{{(0)}} = {lamtree_int}$$""" if lamtree_int else ''
    lamtree = r"## Leading order result for $\lambda_{hhh}$" if lamtree_ext or lamtree_int else ''

    particle_table = PrettyTable(field_names = partprops(0, header=True), hrules = ALL, max_width=1000, align = 'c', sortby = '$S$')
    particle_table.set_style(MARKDOWN)
    SMparticle_table = PrettyTable(field_names = partprops(0,header=True), hrules = ALL, max_width=1000, align = 'c', sortby = '$S$')
    SMparticle_table.set_style(MARKDOWN)
    printed = []
    for k,p in model.SM_particles.items():
        SMparticle_table.add_row(partprops(p, altname = k, model = model))
        printed.append(p.name)
    for k,p in model.all_particles.items():
        if p.name in printed or p.antiname in printed:
            continue
        particle_table.add_row(partprops(p, model = model))
        printed.append(p.name)

    schemefile = path.join(model.modeldir, 'schemes.yml')
    schemes_table = 'No schemes have been defined so far.'
    if path.isfile(schemefile):
        rows = []
        with open(schemefile, 'r') as f:
            schemes = yaml.load(f, Loader=yaml.FullLoader)
            for scheme_name,scheme in schemes.get('renormalization_schemes', {}).items():
                osfields = [p for p,s in scheme.get('mass_counterterms',{}).items() if s == 'OS']
                osmasses = [ f'${particle_name(model.all_particles[p])} ({parameter_name(model.all_particles[p].mass.texname)})$' for p in osfields]
                osmasses = ', '.join(osmasses) if osmasses else '-'

                custom = scheme.get('custom_CT_hhh', False)
                description = scheme.get('description', None)
                description = f'<br>{description}' if description else ''
                custom = "<div style='width:370px;font-size:10pt;text-align:left;'><pre lang='python'><code>" + custom.replace('\n', '<br>').replace('*', '&#42;') + '</code></pre></div>' if custom else '-'
                rows.append([
                    f'`{scheme_name}`' + description,
                    'OS' if scheme.get('VEV_counterterm', 'MS') == 'OS' else '$\\MS$',
                    osmasses,
                    custom
                    ])
        if rows:
            schemes_table = PrettyTable(field_names = [
                'Scheme name',
                'SM VEV',
                'on-shell masses',
                'custom counter term'],
                hrules = ALL,
                max_width=10000,
                min_widht = 20,
                align = 'c',
                sortby = 'Scheme name')
            schemes_table.add_rows(rows)
            schemes_table.set_style(MARKDOWN)
            schemes_table = schemes_table.get_string()

    parameters_table = PrettyTable(field_names = ['UFO parameter', 'value'],
        hrules = ALL,
        align = 'c',
        sortby = 'UFO parameter')
    for p in model.internal_parameters:
        parameters_table.add_row([ f'{p.name} (${parameter_name(p.texname)})$', '$'+latex(model.sympify(str(p.value)), symbol_names=model.texnames)+'$'])
    parameters_table = parameters_table.get_html_string()

    eparameters_table = PrettyTable(field_names = ['input', 'value'],
        hrules = ALL,
        align = 'c',
        sortby = 'input')
    for p in model.external_parameters:
        eparameters_table.add_row([ f'{p.name} (${parameter_name(p.texname)})$', '$'+latex(model.sympify(str(p.value), rational = False), symbol_names=model.texnames)+'$'])
    eparameters_table = eparameters_table.get_html_string()

    with open(Path(m) / ".details.md", 'w') as f:
        test = fr"""
# Automatically generated content
{lamtree}
{lamtree_int}
{lamtree_ext}

## Available renormalization schemes
{schemes_table}

## Particle content

Particles identified as SM-like:

{SMparticle_table.get_string()}

all other particles:

{particle_table.get_string()}

## Input parameters
{eparameters_table}
## Relations between Lagrangian and input parameters
{parameters_table}
"""
        f.write(test)
