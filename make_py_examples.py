#!/usr/bin/env python3
from subprocess import Popen, PIPE, STDOUT, TimeoutExpired
from shutil import which
from glob import glob
from pathlib import Path
from os.path import exists
global nbc

nbapp = which('jupyter-nbconvert')

PYEXAMPLES = 'examples/PythonExamples'

def nbconvert(notebook):
    print('Converting ', notebook, ' to markdown')
    out = notebook.replace('ipynb', 'md')
    proc = Popen(
            [nbapp, '--to', 'markdown', '--template', f'{PYEXAMPLES}/markdown_template', notebook],
            stderr=STDOUT,
            stdout=PIPE,
            cwd=Path(__file__).parent
    )
    try:
        stdout, stderr = proc.communicate(timeout=10)
        print(stdout.decode('utf8').strip())
        rc = proc.poll()
        if rc != 0 or not exists(out):
            print('Notebook conversion failed!', stderr)
            return
    except TimeoutExpired:
        print('Notebook conversion failed (timeout)!', stderr)
        return
    return out

if nbapp:
    for n in glob(f'{PYEXAMPLES}/*ipynb'):
        mdfile = nbconvert(n)
        with open(mdfile.replace(".md", ".py"), 'w+') as f:
            mdfilename = Path(mdfile).absolute().name
            f.write(f"__doc__ = \"\"\"\n.. include:: {mdfilename}\n\"\"\"")
else:
    print('No installation of jupyter-nbconvert found!')
