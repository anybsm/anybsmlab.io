#!/usr/bin/env python3
from pathlib import Path
from shutil import rmtree
from os import makedirs
from pdoc import pdoc, render
import anyBSM # noqa: F401
docs = Path('./docs')
rmtree(docs, ignore_errors=True)
makedirs(docs)

render.configure(
        math = True,
        docformat = 'google',
        template_directory = 'templates',
        logo = 'https://gitlab.com/anybsm/anybsm/-/raw/main/anyBSM/logos/anyH3_logo_small.png?inline=false',
        logo_link = 'https://anybsm.gitlab.io/',
        edit_url_map = {
            "anyBSM": "https://gitlab.com/anybsm/anybsm/blob/main/anyBSM/",
            "models": "https://gitlab.com/anybsm/anybsm_models/blob/main/",
            "examples": "https://gitlab.com/anybsm/anybsm_examples/blob/main/",
            "_mainpage": "https://gitlab.com/anybsm/anybsm.gitlab.io/blob/main/"
            }
        )

pdoc("/_mainpage","anyBSM", '/examples', '/models', output_directory=docs)

makedirs(docs / 'anybsm', exist_ok = True)

old_index = docs / 'anybsm' / 'index.html'
if old_index.is_file():
    print('index.html already exists!')
else:
    with open(old_index,'w') as f:
        f.write('<meta http-equiv="Refresh" content="0; url=\'https://anybsm.gitlab.io\'" />\n')
        f.write('moved to https://anybsm.gitlab.io\n')
