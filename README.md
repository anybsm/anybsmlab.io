This repository hosts the [anyBSM webpage](https://anybsm.gitlab.io).  
To build it locally using [pdoc](https://pdoc.dev/), run `./build_locally` and open `docs/index.html` with your browser.
