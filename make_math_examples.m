(*
    This script converts all Mathematica example notebooks into Markdown.
     -> Input cells are converted into markdown code blocks.
     -> Output cells are stored as PNG and included into the markdown file
*)

SetDirectory["examples/MathematicaExamples"];

InputCellToString[cellexpr_]:=StringReplace[StringJoin[ToString[#]&/@Flatten[cellexpr//.RowBox[a__]:>{a}//.BoxData[a__]:>{a}]],"\[IndentingNewLine]"->"\n"]//StringTrim;

Convert[nb_]:=Block[{nbi,outdir,out,fname,fpath,cell,files=False},
Print["converting ", nb, " to markdown"];
nbi=NotebookImport[nb,_->"Cell"];
outdir="./"<>StringReplace[nb,".nb"->"_files/"];
If[!DirectoryQ[outdir],CreateDirectory[outdir]];
If[!DirectoryQ["../../math_export"],CreateDirectory["../../math_export"]];

out=StringReplace[nb,".nb"->".py"];
If[FileExistsQ[out],DeleteFile[out]];
Print["saving input cells to to ",out];
Print["saving outputcells to ", outdir];
WriteString[out,"__doc__= r\"\"\"\n"];
Do[
If[cell[[2]]=!="Input",
fname = outdir<>(ExpressionUUID/.List@@cell[[3;;]])<>".png";
fpath=Export[fname,cell];
files=True;
WriteString[out,OutputForm["\n![png]("<>fname<>")\n"]];
,
WriteString[out,OutputForm["\n<div class=\"inout\">In[ ]:=</div>\n```\n"<>InputCellToString[cell[[1]]]<>"\n```"]];
],{cell,nbi}];
WriteString[out,"\n\"\"\""];
Close[out];
Print[outdir,"--->","../../math_export/"<>outdir];
If[files,RenameDirectory[outdir,"../../math_export/"<>outdir]];
RenameFile[out,"../../math_export/"<>out];
];

Do[Convert[f];,{f,FileNames["*.nb"]}];
